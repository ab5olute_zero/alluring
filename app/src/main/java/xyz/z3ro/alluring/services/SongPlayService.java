package xyz.z3ro.alluring.services;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import xyz.z3ro.alluring.models.SongModel;

public class SongPlayService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    private final IBinder songBind = new SongPlayBinder();
    //media player
    private MediaPlayer mPlayer;
    //song list
    private ArrayList<SongModel> mSongsList;
    //current position
    private long mSongPosn;
    private boolean mSongSelected;
    private String TAG = "SongPlayService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return songBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mPlayer.stop();
        mPlayer.release();
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSongPosn = 0;
        mSongSelected = false;
        mPlayer = new MediaPlayer();
        initMusicPlayer();
    }

    /**
     * Function to initialize music player service and set properties.
     */
    public void initMusicPlayer() {
        //set player properties
        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);
    }

    /**
     * Function to pass list of songs from Activity/Fragment.
     */
    public void setList(ArrayList<SongModel> theSongs) {
        mSongsList = theSongs;
    }

    /**
     * Function to Play song.
     */
    public void playSong() {
        mPlayer.reset();
//        SongModel song = mSongsList.get(mSongPosn);
        long currSong = mSongPosn;
        Uri trackUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong);
        try {
            mPlayer.setDataSource(getApplicationContext(), trackUri);
        } catch (Exception e) {
            Log.e(TAG, "Error setting data source", e);
        }
        mPlayer.prepareAsync();
    }

    /**
     * Function to pause song
     */
    public void pauseSong() {
        mPlayer.pause();
    }

    /**
     * Function to resume song after it is paused
     */
    public void resumeSong() {
        mPlayer.start();
    }

    /**
     * Function to get playback state
     */
    public boolean playbackState() {
        return mPlayer.isPlaying();
    }

    /**
     * Function to check if a song is set
     */
    public boolean checkSongSet() {
        return mSongSelected;
    }

    /**
     * Function to set song.
     */
    public void setSong(long songIndex) {
        mSongPosn = songIndex;
        mSongSelected = true;
    }

    /**
     * Implemented Functions
     */
    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    public class SongPlayBinder extends Binder {
        public SongPlayService getService() {
            return SongPlayService.this;
        }
    }
}
