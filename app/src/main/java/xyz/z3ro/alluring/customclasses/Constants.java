package xyz.z3ro.alluring.customclasses;

public class Constants {
    public static final int SCROLL_DIRECTION_UP = -1;
    public static final String ALBUM_ID_KEY = "album_id";
    public static final String ARTIST_ID_KEY = "artist_id";
    public static final String GENRE_ID_KEY = "genre_id";
}
