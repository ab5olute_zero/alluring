package xyz.z3ro.alluring.customclasses;

import java.util.concurrent.TimeUnit;

public class SongUtility {

    public static int getTrackNumber(int trackNumber) {
        if (trackNumber > 999) {
            return trackNumber % 1000;
        } else
            return trackNumber;
    }

    public static String parseTime(long milliseconds) {
        String FORMAT1 = "%02d:%02d:%02d";
        String FORMAT2 = "%02d:%02d";
        if (TimeUnit.MILLISECONDS.toHours(milliseconds) > 0)
            return String.format(FORMAT1,
                    TimeUnit.MILLISECONDS.toHours(milliseconds),
                    TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(milliseconds)),
                    TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
        else
            return String.format(FORMAT2, TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(
                    TimeUnit.MILLISECONDS.toHours(milliseconds)),
                    TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }
}
