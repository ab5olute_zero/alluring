package xyz.z3ro.alluring.models;

public class GenreModel {

    private long id;
    private String genre;

    public GenreModel(long genreId, String genreName) {
        this.id = genreId;
        this.genre = genreName;
    }

    public long getID() {
        return id;
    }

    public String getGenre() {
        return genre;
    }
}
