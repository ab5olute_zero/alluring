package xyz.z3ro.alluring.models;

public class AlbumModel {
    private long id;
    private String title;
    private String artist;
    private String albumArt;

    public AlbumModel(long albumId, String albumTitle, String albumArtist, String theAlbumArt) {
        this.id = albumId;
        this.title = albumTitle;
        this.artist = albumArtist;
        this.albumArt = theAlbumArt;
    }

    public AlbumModel(long albumId, String albumTitle, String theAlbumArt) {
        this.id = albumId;
        this.title = albumTitle;
        this.albumArt = theAlbumArt;
    }

    public long getID() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbumArt() {
        return albumArt;
    }
}
