package xyz.z3ro.alluring.models;

public class PlaylistModel {

    private long mId;
    private String mName;

    public PlaylistModel(long id, String name) {
        this.mId = id;
        this.mName = name;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
