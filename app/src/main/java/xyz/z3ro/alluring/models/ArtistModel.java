package xyz.z3ro.alluring.models;

public class ArtistModel {

    private long id;
    private String name;
    private String numberOfAlbums;
    private String numberOfSongs;

    public ArtistModel(long artistId, String artistName, String noOfAlbums, String noOfSongs) {
        this.id = artistId;
        this.name = artistName;
        this.numberOfAlbums = noOfAlbums;
        this.numberOfSongs = noOfSongs;
    }

    public long getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumberOfAlbums() {
        return numberOfAlbums;
    }

    public String getNumberOfSongs() {
        return numberOfSongs;
    }

}
