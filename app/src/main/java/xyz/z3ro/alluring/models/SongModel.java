package xyz.z3ro.alluring.models;

public class SongModel implements Comparable, Cloneable {
    private long mID;
    private String mTitle;
    private String mArtist;
    private long mDuration;
    private int mTrackNumber;

    public SongModel(long songId, String songTitle, String songArtist) {
        this.mID = songId;
        this.mTitle = songTitle;
        this.mArtist = songArtist;
    }

    public SongModel(long songId, String songTitle, long duration, int trackNumber) {
        this.mID = songId;
        this.mTitle = songTitle;
        this.mDuration = duration;
        this.mTrackNumber = trackNumber;
    }

    public SongModel(long songId, String songTitle, long duration) {
        this.mID = songId;
        this.mTitle = songTitle;
        this.mDuration = duration;
    }

    public long getID() {
        return mID;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getArtist() {
        return mArtist;
    }

    public long getDuration() {
        return mDuration;
    }

    public int getTrackNumber() {
        return mTrackNumber;
    }

    @Override
    public int compareTo(Object o) {
        SongModel compare = (SongModel) o;

        if (compare.mID == this.mID)
            return 0;
        return 1;
    }

    @Override
    public SongModel clone() {
        SongModel clone;

        try {
            clone = (SongModel) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }

        return clone;
    }
}