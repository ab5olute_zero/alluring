package xyz.z3ro.alluring.fragments.TabsNavigation;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.adapters.SongAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;
import xyz.z3ro.alluring.services.SongPlayService;

public class SongsFragments extends Fragment {

    private View mRootView;

    private RecyclerView songRecyclerView;
    private ArrayList<SongModel> songList;
    private SongAdapter mSongAdapter;

    private FloatingActionButton floatingActionButton;
    private TextView textView;

    private SongPlayService songPlayService;
    private Intent playIntent;
    private boolean songBound = false;
    private long DEFAULT_ID;
    private ServiceConnection songConnection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_songs, container, false);
        songRecyclerView = mRootView.findViewById(R.id.songsRecyclerView);
        floatingActionButton = getActivity().findViewById(R.id.floatingActionButton_playbackControl);
        textView = getActivity().findViewById(R.id.textView_nowPlaying);
        songList = new ArrayList<SongModel>();
        DEFAULT_ID = 0;
        getSongList();

        mSongAdapter = new SongAdapter(songList, new SongItemClickListener() {
            @Override
            public void onItemClick(SongModel song) {
                if (songPlayService != null) {
                    songPlayService.setSong(song.getID());
                    songPlayService.playSong();
                    floatingActionButton.setImageResource(R.drawable.ic_pause_white_24dp);
                    textView.setText(song.getTitle());
                }

            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (songPlayService.checkSongSet()) {
                    if (songPlayService.playbackState()) {
                        floatingActionButton.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                        songPlayService.pauseSong();
                    } else {
                        floatingActionButton.setImageResource(R.drawable.ic_pause_white_24dp);
                        songPlayService.resumeSong();
                    }
                } else {
                    songPlayService.setSong(DEFAULT_ID);
                    songPlayService.playSong();
                    floatingActionButton.setImageResource(R.drawable.ic_pause_white_24dp);
                }
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mRootView.getContext());
        songRecyclerView.setLayoutManager(layoutManager);
        songRecyclerView.setItemAnimator(new DefaultItemAnimator());
        songRecyclerView.setAdapter(mSongAdapter);
        songRecyclerView.setItemViewCacheSize(5);

        final AppBarLayout appBarLayout = getActivity().findViewById(R.id.appbar);
        songRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.canScrollVertically(Constants.SCROLL_DIRECTION_UP))
                    appBarLayout.setElevation(8);
                else
                    appBarLayout.setElevation(0);
            }
        });


        songConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                SongPlayService.SongPlayBinder songPlayBinder = (SongPlayService.SongPlayBinder) iBinder;
                songPlayService = songPlayBinder.getService();
                songPlayService.setList(songList);
                songBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                songBound = false;
            }
        };
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(getActivity(), SongPlayService.class);
            getActivity().bindService(playIntent, songConnection, Context.BIND_AUTO_CREATE);
            getActivity().startService(playIntent);
        }
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy() {
        getActivity().stopService(playIntent);
        songPlayService = null;
        super.onDestroy();
    }

    /**
     * Helper function to retrieve SongsFragments List with Title and Artist's name
     */
    private void getSongList() {
        ContentResolver songResolver;
        Uri songUri;
        Cursor songCursor;
        try {
            songResolver = getActivity().getContentResolver();
        } catch (NullPointerException e) {
            e.printStackTrace();
            songResolver = null;
        }
        if (songResolver != null) {
            songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String[] projection = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.ARTIST};
            String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
            songCursor = songResolver.query(songUri, projection, null, null, sortOrder);
        } else {
            songCursor = null;
        }
        if (songCursor != null && songCursor.moveToFirst()) {
            //get columns
            int idColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int artistColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            DEFAULT_ID = songCursor.getLong(idColumn);
            do {
                long thisId = songCursor.getLong(idColumn);
                String thisTitle = songCursor.getString(titleColumn);
                String thisArtist = songCursor.getString(artistColumn);
                songList.add(new SongModel(thisId, thisTitle, thisArtist));
            }
            while (songCursor.moveToNext());
        }
        if (songCursor != null)
            songCursor.close();
    }
}
