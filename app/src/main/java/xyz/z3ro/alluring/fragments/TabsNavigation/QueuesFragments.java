package xyz.z3ro.alluring.fragments.TabsNavigation;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.adapters.PlaylistAdapter;
import xyz.z3ro.alluring.interfaces.PlaylistItemClickListener;
import xyz.z3ro.alluring.models.PlaylistModel;

public class QueuesFragments extends Fragment {
    private View mRootview;
    private ArrayList<PlaylistModel> playlistModels;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootview = inflater.inflate(R.layout.tab_queues, container, false);
        RecyclerView playListRecyclerView = mRootview.findViewById(R.id.recyclerView_Playlist);
        playlistModels = new ArrayList<PlaylistModel>();

        // Get playlist
        getPlaylist();

        PlaylistAdapter playlistAdapter = new PlaylistAdapter(playlistModels, new PlaylistItemClickListener() {
            @Override
            public void onItemClick(PlaylistModel playlistModel) {
                Toast.makeText(getActivity(), "Now Playing " + playlistModel.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mRootview.getContext());
        playListRecyclerView.setLayoutManager(layoutManager);
        playListRecyclerView.setAdapter(playlistAdapter);
        playListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        playListRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        playListRecyclerView.setItemViewCacheSize(2);

        return mRootview;
    }


    /**
     * Helper function to get all custom playlist from device
     */
    private void getPlaylist() {
        ContentResolver playlistResolver;
        Cursor playlistCursor;
        Uri playlistUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Playlists._ID, MediaStore.Audio.Playlists.NAME};
        String sortOrder = MediaStore.Audio.Playlists.NAME + " ASC";

        try {
            playlistResolver = getActivity().getContentResolver();
        } catch (NullPointerException e) {
            e.printStackTrace();
            playlistResolver = null;
        }

        if (playlistResolver != null) {
            playlistCursor = playlistResolver.query(playlistUri, projection, null, null, sortOrder);
        } else
            playlistCursor = null;

        if (playlistCursor != null && playlistCursor.moveToFirst()) {
            // Get columns
            int idColumn = playlistCursor.getColumnIndex(MediaStore.Audio.Playlists._ID);
            int nameColumn = playlistCursor.getColumnIndex(MediaStore.Audio.Playlists.NAME);

            do {
                long thisId = playlistCursor.getLong(idColumn);
                String thisName = playlistCursor.getString(nameColumn);
                playlistModels.add(new PlaylistModel(thisId, thisName));
            }
            while (playlistCursor.moveToNext());

            playlistCursor.close();
        }
    }
}
