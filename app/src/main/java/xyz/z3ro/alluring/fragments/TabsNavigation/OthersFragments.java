package xyz.z3ro.alluring.fragments.TabsNavigation;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewSwitcher;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.activities.ArtistActivity;
import xyz.z3ro.alluring.activities.GenreActivity;
import xyz.z3ro.alluring.adapters.ArtistAdapter;
import xyz.z3ro.alluring.adapters.GenreAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.ArtistItemClickListener;
import xyz.z3ro.alluring.interfaces.GenreItemClickListener;
import xyz.z3ro.alluring.models.ArtistModel;
import xyz.z3ro.alluring.models.GenreModel;

public class OthersFragments extends Fragment {
    private View mRootView;
    private RecyclerView mArtistsRecyclerView;
    private ArrayList<ArtistModel> artistList;

    private RecyclerView mGenresRecyclerView;
    private ArrayList<GenreModel> genreList;

    private AppBarLayout appBarLayout;
    private AppBarLayout othersAppBar;

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     *
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_others, container, false);

        Chip artistChip = mRootView.findViewById(R.id.chip_artist);
        Chip genreChip = mRootView.findViewById(R.id.chip_genres);
        appBarLayout = getActivity().findViewById(R.id.appbar);
        othersAppBar = mRootView.findViewById(R.id.others_appBar);

        final ViewSwitcher viewSwitcher = mRootView.findViewById(R.id.viewSwitcher_others);

        artistChip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewSwitcher.setDisplayedChild(0);
            }
        });

        genreChip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewSwitcher.setDisplayedChild(1);
            }
        });

        // Set up Artists list
        setUpArtistTab();

        // Set up Genres list
        setUpGenresTab();


        return mRootView;
    }

    /**
     * Helper function to set up Artists Tab
     */
    private void setUpArtistTab() {
        mArtistsRecyclerView = mRootView.findViewById(R.id.recyclerView_artists);
        artistList = new ArrayList<ArtistModel>();
        getArtistList();
        Collections.sort(artistList, new Comparator<ArtistModel>() {
            @Override
            public int compare(ArtistModel artistModel, ArtistModel t1) {
                return artistModel.getName().compareTo(t1.getName());
            }
        });
        ArtistAdapter artistAdapter = new ArtistAdapter(artistList, new ArtistItemClickListener() {
            @Override
            public void onItemClick(ArtistModel artistModel) {
                Intent intent = new Intent(getActivity(), ArtistActivity.class);
                intent.putExtra(Constants.ARTIST_ID_KEY, artistModel.getID());
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mRootView.getContext());
        mArtistsRecyclerView.setLayoutManager(layoutManager);
        mArtistsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mArtistsRecyclerView.setAdapter(artistAdapter);
        mArtistsRecyclerView.setItemViewCacheSize(5);
        mArtistsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.canScrollVertically(Constants.SCROLL_DIRECTION_UP)) {
                    othersAppBar.setElevation(8);
                } else {
                    appBarLayout.setElevation(0);
                    othersAppBar.setElevation(0);
                }
            }
        });
    }

    /**
     * Helper method to retrieve ArtistFragment List with Artist's name and detail
     */
    private void getArtistList() {
        ContentResolver artistResolver;
        Uri artistUri;
        Cursor artistCursor;
        try {
            artistResolver = getActivity().getContentResolver();
        } catch (NullPointerException e) {
            e.printStackTrace();
            artistResolver = null;
        }
        if (artistResolver != null) {
            artistUri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
            String[] projection = new String[]{MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST,
                    MediaStore.Audio.Artists.NUMBER_OF_ALBUMS, MediaStore.Audio.Artists.NUMBER_OF_TRACKS};
            artistCursor = artistResolver.query(artistUri, projection, null, null, null);
        } else {
            artistCursor = null;
        }
        if (artistCursor != null && artistCursor.moveToFirst()) {
            //get columns
            int idColumn = artistCursor.getColumnIndex(MediaStore.Audio.Artists._ID);
            int titleColumn = artistCursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
            int detailAlbumColumn = artistCursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS);
            int detailTrackColumn = artistCursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_TRACKS);
            do {
                long thisId = artistCursor.getLong(idColumn);
                String thisTitle = artistCursor.getString(titleColumn);
                String thisDetailAlbum = artistCursor.getString(detailAlbumColumn);
                String thisDetailTrack = artistCursor.getString(detailTrackColumn);
                artistList.add(new ArtistModel(thisId, thisTitle, thisDetailAlbum, thisDetailTrack));
            }
            while (artistCursor.moveToNext());
        }
        artistCursor.close();
    }

    /**
     * Helper function to set up Genres Tab
     */
    private void setUpGenresTab() {
        mGenresRecyclerView = mRootView.findViewById(R.id.recyclerView_genres);
        genreList = new ArrayList<GenreModel>();
        getGenreList();
        Collections.sort(genreList, new Comparator<GenreModel>() {
            @Override
            public int compare(GenreModel genreModel, GenreModel t1) {
                return genreModel.getGenre().compareTo(t1.getGenre());
            }
        });
        GenreAdapter genreAdapter = new GenreAdapter(genreList, new GenreItemClickListener() {
            @Override
            public void onItemClick(GenreModel genreModel) {
                Intent intent = new Intent(getActivity(), GenreActivity.class);
                intent.putExtra(Constants.GENRE_ID_KEY, genreModel.getID());
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mRootView.getContext());
        mGenresRecyclerView.setLayoutManager(layoutManager);
        mGenresRecyclerView.setAdapter(genreAdapter);
        mGenresRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mGenresRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mGenresRecyclerView.setItemViewCacheSize(5);
        mGenresRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.canScrollVertically(Constants.SCROLL_DIRECTION_UP)) {
                    othersAppBar.setElevation(8);
                } else {
                    appBarLayout.setElevation(0);
                    othersAppBar.setElevation(0);
                }
            }
        });
    }

    /**
     * Helper function to retrieve GenreFragment List with Genre's name and detail
     */
    private void getGenreList() {
        ContentResolver genreResolver;
        Uri genreUri;
        Cursor genreCursor;
        try {
            genreResolver = getActivity().getContentResolver();
        } catch (NullPointerException e) {
            e.printStackTrace();
            genreResolver = null;
        }
        if (genreResolver != null) {
            genreUri = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
            String[] projection = new String[]{MediaStore.Audio.Genres._ID, MediaStore.Audio.Genres.NAME};
            genreCursor = genreResolver.query(genreUri, projection, null, null, null);
        } else {
            genreCursor = null;
        }
        if (genreCursor != null && genreCursor.moveToFirst()) {
            //get columns
            int idColumn = genreCursor.getColumnIndex(MediaStore.Audio.Genres._ID);
            int titleColumn = genreCursor.getColumnIndex(MediaStore.Audio.Genres.NAME);
            do {
                long thisId = genreCursor.getLong(idColumn);
                String thisTitle = genreCursor.getString(titleColumn);
                genreList.add(new GenreModel(thisId, thisTitle));
            }
            while (genreCursor.moveToNext());
        }
        genreCursor.close();
    }

}
