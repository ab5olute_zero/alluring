package xyz.z3ro.alluring.fragments.TabsNavigation;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.activities.AlbumActivity;
import xyz.z3ro.alluring.adapters.AlbumAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.AlbumItemClickListener;
import xyz.z3ro.alluring.models.AlbumModel;

public class AlbumsFragments extends Fragment {

    private View mRootView;
    private ArrayList<AlbumModel> albumList;

    private int NO_OF_COLUMNS = 2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_albums, container, false);
        RecyclerView albumsRecyclerView = mRootView.findViewById(R.id.albumsRecyclerView);
        albumList = new ArrayList<AlbumModel>();
        getAlbumList();
        Collections.sort(albumList, new Comparator<AlbumModel>() {
            @Override
            public int compare(AlbumModel albumModel, AlbumModel t1) {
                return albumModel.getTitle().compareTo(t1.getTitle());
            }
        });
        AlbumAdapter albumAdapter = new AlbumAdapter(albumList, new AlbumItemClickListener() {
            @Override
            public void onItemClick(AlbumModel album) {
                Toast.makeText(getActivity(), "Now Playing " + album.getAlbumArt(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), AlbumActivity.class);
                intent.putExtra(Constants.ALBUM_ID_KEY, album.getID());
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(mRootView.getContext(), NO_OF_COLUMNS);
        albumsRecyclerView.setLayoutManager(layoutManager);
        albumsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        albumsRecyclerView.setAdapter(albumAdapter);
        albumsRecyclerView.setItemViewCacheSize(8);

        final AppBarLayout appBarLayout = getActivity().findViewById(R.id.appbar);
        albumsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.canScrollVertically(Constants.SCROLL_DIRECTION_UP))
                    appBarLayout.setElevation(8);
                else
                    appBarLayout.setElevation(0);
            }
        });

        return mRootView;
    }

    /**
     * Helper method to retrieve SongsFragments List with Title and Artist's name
     */
    private void getAlbumList() {
        ContentResolver albumResolver;
        Uri albumUri;
        Cursor albumCursor;
        try {
            albumResolver = getActivity().getContentResolver();
        } catch (NullPointerException e) {
            e.printStackTrace();
            albumResolver = null;
        }
        if (albumResolver != null) {
            albumUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
            String[] projection = new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM,
                    MediaStore.Audio.Albums.ARTIST, MediaStore.Audio.Albums.ALBUM_ART, MediaStore.Audio.Albums.NUMBER_OF_SONGS};
            albumCursor = albumResolver.query(albumUri, projection, null, null, null);
        } else {
            albumCursor = null;
        }
        if (albumCursor != null && albumCursor.moveToFirst()) {
            //get columns
            int idColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums._ID);
            int titleColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
            int artistColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
            int albumArtColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            do {
                long thisId = albumCursor.getLong(idColumn);
                String thisTitle = albumCursor.getString(titleColumn);
                String thisArtist = albumCursor.getString(artistColumn);
                String thisAlbumArt = albumCursor.getString(albumArtColumn);
                albumList.add(new AlbumModel(thisId, thisTitle, thisArtist, thisAlbumArt));
            }
            while (albumCursor.moveToNext());
        }
        if (albumCursor != null)
            albumCursor.close();
    }
}
