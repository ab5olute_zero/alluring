package xyz.z3ro.alluring;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.ColorRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import xyz.z3ro.alluring.activities.SearchActivity;
import xyz.z3ro.alluring.fragments.TabsNavigation.AlbumsFragments;
import xyz.z3ro.alluring.fragments.TabsNavigation.OthersFragments;
import xyz.z3ro.alluring.fragments.TabsNavigation.QueuesFragments;
import xyz.z3ro.alluring.fragments.TabsNavigation.SongsFragments;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, TabLayout.BaseOnTabSelectedListener {

    private TextView mHeaderText;
    private TabLayout mTabLayout;

    private static final String TAG = "MainActivity";

    private static final int DEFAULT_TAB = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        // Set Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        // Set Bottom AppBar
        setBottomAppBar();

        // Set header text
        mHeaderText = findViewById(R.id.header_text);
        mHeaderText.setTextColor(fetchColor(R.color.colorBottomMenuHome));

        // Check for Read External Storage permission
        if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            HomeInitialization();
        } else {
            Log.v(TAG, "Permission is revoked");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 777);
        }


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1 || getSupportFragmentManager().getBackStackEntryCount() == 2)
            finish();
        else
            super.onBackPressed();
    }

    /**
     * Permission request handler
     *
     * @param requestCode  to handle permission request
     * @param permissions  array of permissions
     * @param grantResults array of results of permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            HomeInitialization();
        } else {

        }
    }

    /**
     * Set toolbar menu
     *
     * @param menu menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_top_app_bar, menu);
        return true;
    }

    /**
     * Callback method for toolbar menu items
     *
     * @param item menu items
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, as long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.top_search:
                Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(searchIntent);
                break;
            case (R.id.top_settings):
                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * ViewPager/Tabs initialisation
     */
    private void HomeInitialization() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.tab_container);
        viewPager.setAdapter(sectionsPagerAdapter);

        mTabLayout = findViewById(R.id.tabs);

        viewPager.addOnPageChangeListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        viewPager.setCurrentItem(DEFAULT_TAB, true);
        viewPager.setOffscreenPageLimit(3);
        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        mTabLayout.addOnTabSelectedListener(this);
    }

    /**
     * This method will be invoked when the current page is scrolled, either as part
     * of a programmatically initiated smooth scroll or a user initiated touch scroll.
     *
     * @param position             Position index of the first page currently being displayed.
     *                             Page position+1 will be visible if positionOffset is nonzero.
     * @param positionOffset       Value from [0, 1) indicating the offset from the page at position.
     * @param positionOffsetPixels Value in pixels indicating the offset from position.
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    /**
     * This method will be invoked when a new page becomes selected. Animation is not
     * necessarily complete.
     *
     * @param position Position index of the new selected page.
     */
    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                mHeaderText.setText(R.string.tab_text_1);
                break;
            case 1:
                mHeaderText.setText(R.string.tab_text_2);
                break;
            case 2:
                mHeaderText.setText(R.string.tab_text_3);
                break;
            case 3:
                mHeaderText.setText(R.string.tab_text_4);
                break;
            default:
                mHeaderText.setText(R.string.app_name);

        }
    }

    /**
     * Called when the scroll state changes. Useful for discovering when the user
     * begins dragging, when the pager is automatically settling to the current page,
     * or when it is fully stopped/idle.
     *
     * @param state The new scroll state.
     * @see ViewPager#SCROLL_STATE_IDLE
     * @see ViewPager#SCROLL_STATE_DRAGGING
     * @see ViewPager#SCROLL_STATE_SETTLING
     */
    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                tab.setIcon(R.drawable.ic_queue_black_24dp);
                break;
            case 1:
                tab.setIcon(R.drawable.ic_library_music_black_24dp);
                break;
            case 2:
                tab.setIcon(R.drawable.ic_album_black_24dp);
                break;
            case 3:
                tab.setIcon(R.drawable.ic_queue_music_black_24dp);
                break;
            default:
                tab.setIcon(R.drawable.ic_music_note_black_24dp);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                tab.setIcon(R.drawable.ic_queue_grey_24dp);
                break;
            case 1:
                tab.setIcon(R.drawable.ic_library_music_grey_24dp);
                break;
            case 2:
                tab.setIcon(R.drawable.ic_album_grey_24dp);
                break;
            case 3:
                tab.setIcon(R.drawable.ic_queue_music_grey_24dp);
                break;
            default:
                tab.setIcon(R.drawable.ic_music_note_black_24dp);
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                tab.setIcon(R.drawable.ic_queue_black_24dp);
                break;
            case 1:
                tab.setIcon(R.drawable.ic_library_music_black_24dp);
                break;
            case 2:
                tab.setIcon(R.drawable.ic_album_black_24dp);
                break;
            case 3:
                tab.setIcon(R.drawable.ic_queue_music_black_24dp);
                break;
            default:
                tab.setIcon(R.drawable.ic_music_note_black_24dp);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new QueuesFragments();
                case 1:
                    return new SongsFragments();
                case 2:
                    return new AlbumsFragments();
                case 3:
                    return new OthersFragments();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }
    }


    /**
     * Helper method to set up BottomAppBar.
     * Listens to touch events and swipe gestures.
     */
    private void setBottomAppBar() {
        BottomAppBar bottomAppBar = findViewById(R.id.bottom_app_bar);
        bottomAppBar.replaceMenu(R.menu.menu_bottom_app_bar);
        bottomAppBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.bottom_repeat):

                        break;
                    case (R.id.bottom_shuffle):

                        break;
                }
                return false;
            }
        });
        bottomAppBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        bottomAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Navigation OnClickListener", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Fetch color
     *
     * @param color color resource
     */
    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

}
