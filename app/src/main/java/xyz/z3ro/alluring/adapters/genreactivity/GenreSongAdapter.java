package xyz.z3ro.alluring.adapters.genreactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.customclasses.SongUtility;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class GenreSongAdapter extends RecyclerView.Adapter<GenreSongAdapter.GenreSongViewHolder> {
    private ArrayList<SongModel> songs;
    private SongItemClickListener songItemClickListener;

    public GenreSongAdapter(ArrayList<SongModel> songModels, SongItemClickListener songItemClickListener) {
        this.songs = songModels;
        this.songItemClickListener = songItemClickListener;
    }

    @NonNull
    @Override
    public GenreSongAdapter.GenreSongViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_genre_song_item, viewGroup, false);
        return new GenreSongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreSongAdapter.GenreSongViewHolder genreSongViewHolder, int i) {
        SongModel songModel = songs.get(i);
        genreSongViewHolder.songTitle.setText(songModel.getTitle());
        genreSongViewHolder.songDuration.setText(SongUtility.parseTime(songModel.getDuration()));

        genreSongViewHolder.bind(songs.get(i),songItemClickListener);
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class GenreSongViewHolder extends RecyclerView.ViewHolder {
        public TextView songTitle;
        public TextView songDuration;

        public GenreSongViewHolder(View view) {
            super(view);
            songTitle = view.findViewById(R.id.textView_genre_SongTitle);
            songDuration = view.findViewById(R.id.textView_genre_songDuration);
        }

        public void bind(final SongModel songModel, final SongItemClickListener songItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songItemClickListener.onItemClick(songModel);
                }
            });
        }
    }
}
