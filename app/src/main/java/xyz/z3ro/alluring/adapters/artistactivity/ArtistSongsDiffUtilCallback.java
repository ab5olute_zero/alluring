package xyz.z3ro.alluring.adapters.artistactivity;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import xyz.z3ro.alluring.models.SongModel;

public class ArtistSongsDiffUtilCallback extends DiffUtil.Callback {

    private ArrayList<SongModel> newList;
    private ArrayList<SongModel> oldList;

    public ArtistSongsDiffUtilCallback(ArrayList<SongModel> newList, ArrayList<SongModel> oldList) {
        this.newList = newList;
        this.oldList = oldList;
    }

    @Override
    public int getNewListSize() {
        if (newList != null)
            return newList.size();
        return 0;
    }

    @Override
    public int getOldListSize() {
        if (oldList != null)
            return oldList.size();
        return 0;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        int result = newList.get(newItemPosition).compareTo(oldList.get(oldItemPosition));
        return result == 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return newList.get(newItemPosition).getID() == oldList.get(oldItemPosition).getID();
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
//        SongModel newModel = newList.get(newItemPosition);
//        SongModel oldModel = oldList.get(oldItemPosition);
//
//        Bundle diff = new Bundle();
//
//        if (!newModel.getTitle().equals(oldModel.getTitle())) {
//            diff.putString("title", newModel.getTitle());
//        }
//        if (newModel.getDuration() != oldModel.getDuration()) {
//            diff.putLong("duration", newModel.getDuration());
//        }
//
//        if (diff.size() == 0)
//            return null;
//
//        return diff;
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
