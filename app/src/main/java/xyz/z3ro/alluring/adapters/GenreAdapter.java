package xyz.z3ro.alluring.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.GenreItemClickListener;
import xyz.z3ro.alluring.models.GenreModel;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenresViewHolder> {

    private GenreItemClickListener genreItemClickListener;
    private ArrayList<GenreModel> genres;

    public GenreAdapter(ArrayList<GenreModel> theGenre, GenreItemClickListener genreItemClickListener) {
        this.genres = theGenre;
        this.genreItemClickListener = genreItemClickListener;
    }

    @NonNull
    @Override
    public GenreAdapter.GenresViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.genre_list_item, parent, false);
        return new GenresViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreAdapter.GenresViewHolder holder, int position) {
        GenreModel genreModel = genres.get(position);

        holder.genreName.setText(genreModel.getGenre());

        holder.bind(genres.get(position), genreItemClickListener);
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    public class GenresViewHolder extends RecyclerView.ViewHolder {
        public TextView genreName;

        public GenresViewHolder(View view) {
            super(view);
            genreName = view.findViewById(R.id.textView_GenreName);
        }

        public void bind(final GenreModel genreModel, final GenreItemClickListener genreItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    genreItemClickListener.onItemClick(genreModel);
                }
            });
        }
    }
}
