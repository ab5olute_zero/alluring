package xyz.z3ro.alluring.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongsViewHolder> {
    private final SongItemClickListener songItemClickListener;
    private ArrayList<SongModel> songs;

    public SongAdapter(ArrayList<SongModel> theSongs, SongItemClickListener songItemClickListener) {
        this.songs = theSongs;
        this.songItemClickListener = songItemClickListener;
    }

    @NonNull
    @Override
    public SongAdapter.SongsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.song_list_item, viewGroup, false);
        return new SongsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SongsViewHolder songsViewHolder, int i) {
        SongModel song = songs.get(i);
        songsViewHolder.songTitle.setText(song.getTitle());
        songsViewHolder.artistName.setText(song.getArtist());
        songsViewHolder.bind(songs.get(i), songItemClickListener);
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class SongsViewHolder extends RecyclerView.ViewHolder {
        public TextView songTitle;
        public TextView artistName;

        public SongsViewHolder(View view) {
            super(view);
            songTitle = view.findViewById(R.id.textView_SongTitle);
            artistName = view.findViewById(R.id.textView_Song_ArtistName);
        }

        public void bind(final SongModel song, final SongItemClickListener songItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songItemClickListener.onItemClick(song);
                }
            });
        }
    }
}
