package xyz.z3ro.alluring.adapters.artistactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.AlbumItemClickListener;
import xyz.z3ro.alluring.models.AlbumModel;

public class ArtistAlbumAdapter extends RecyclerView.Adapter<ArtistAlbumAdapter.ArtistAlbumViewHolder> {
    private AlbumItemClickListener albumItemClickListener;
    private ArrayList<AlbumModel> albums;

    public ArtistAlbumAdapter(ArrayList<AlbumModel> albumModels, AlbumItemClickListener albumItemClickListener) {
        this.albums = albumModels;
        this.albumItemClickListener = albumItemClickListener;
    }

    @NonNull
    @Override
    public ArtistAlbumAdapter.ArtistAlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_artist_album_item, parent, false);
        return new ArtistAlbumViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistAlbumAdapter.ArtistAlbumViewHolder holder, int position) {
        AlbumModel albumModel = albums.get(position);
        holder.albumName.setText(albumModel.getTitle());

        if (albumModel.getAlbumArt() != null)
            Picasso.get().load(new File(albumModel.getAlbumArt())).fit().placeholder(R.drawable.ic_album_grey_24dp)
                    .into(holder.albumArt);
        else
            Picasso.get().load(R.drawable.ic_album_grey_24dp).fit().placeholder(R.drawable.ic_album_grey_24dp)
                    .into(holder.albumArt);
        holder.bind(albums.get(position), albumItemClickListener);
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class ArtistAlbumViewHolder extends RecyclerView.ViewHolder {
        public ImageView albumArt;
        public TextView albumName;

        public ArtistAlbumViewHolder(View view) {
            super(view);
            albumArt = view.findViewById(R.id.imageView_artist_albumArt);
            albumName = view.findViewById(R.id.textView_artist_albumName);
        }

        public void bind(final AlbumModel albumModel, final AlbumItemClickListener albumItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    albumItemClickListener.onItemClick(albumModel);
                }
            });
        }
    }
}
