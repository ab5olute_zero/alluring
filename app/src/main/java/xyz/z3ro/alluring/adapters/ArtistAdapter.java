package xyz.z3ro.alluring.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.ArtistItemClickListener;
import xyz.z3ro.alluring.models.ArtistModel;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistsViewHolder> {

    private ArtistItemClickListener artistItemClickListener;
    private ArrayList<ArtistModel> artists;

    public ArtistAdapter(ArrayList<ArtistModel> theArtists, ArtistItemClickListener artistItemClickListener) {
        this.artists = theArtists;
        this.artistItemClickListener = artistItemClickListener;
    }

    @NonNull
    @Override
    public ArtistAdapter.ArtistsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_list_item, parent, false);
        return new ArtistsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistAdapter.ArtistsViewHolder holder, int position) {
        ArtistModel artistModel = artists.get(position);

        String artistDetail = holder.artistDetail.getContext().getString(R.string.artist_detail
                , artistModel.getNumberOfAlbums(), artistModel.getNumberOfSongs());
        holder.artistName.setText(artistModel.getName());
        holder.artistDetail.setText(artistDetail);

        holder.bind(artists.get(position),artistItemClickListener);
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    public class ArtistsViewHolder extends RecyclerView.ViewHolder {
        public TextView artistName;
        public TextView artistDetail;

        public ArtistsViewHolder(View view) {
            super(view);
            artistName = view.findViewById(R.id.textView_ArtistTitle);
            artistDetail = view.findViewById(R.id.textView_ArtistDetail);
        }

        public void bind(final ArtistModel artistModel, final ArtistItemClickListener artistItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    artistItemClickListener.onItemClick(artistModel);
                }
            });
        }
    }
}
