package xyz.z3ro.alluring.adapters.artistactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.customclasses.SongUtility;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class ArtistSongAdapter extends RecyclerView.Adapter<ArtistSongAdapter.ArtistSongViewHolder> {
    private ArrayList<SongModel> songs;
    private SongItemClickListener songItemClickListener;

    public ArtistSongAdapter(ArrayList<SongModel> songModels, SongItemClickListener songItemClickListener) {
        this.songs = songModels;
        this.songItemClickListener = songItemClickListener;
    }

    @NonNull
    @Override
    public ArtistSongAdapter.ArtistSongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_artist_song_item, parent, false);
        return new ArtistSongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistSongAdapter.ArtistSongViewHolder holder, int position) {
        SongModel songModel = songs.get(position);

        holder.songName.setText(songModel.getTitle());
        holder.songDuration.setText(String.valueOf(SongUtility.parseTime(songModel.getDuration())));

        holder.bind(songs.get(position), songItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistSongViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public ArrayList<SongModel> getData() {
        return songs;
    }

    public void setData(ArrayList<SongModel> newSongs) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ArtistSongsDiffUtilCallback(newSongs, songs));
        diffResult.dispatchUpdatesTo(this);
        songs.clear();
        this.songs.addAll(newSongs);
    }

    public class ArtistSongViewHolder extends RecyclerView.ViewHolder {

        public TextView songName;
        public TextView songDuration;

        public ArtistSongViewHolder(View view) {
            super(view);
            songName = view.findViewById(R.id.textView_artist_SongTitle);
            songDuration = view.findViewById(R.id.textView_artist_songDuration);
        }

        public void bind(final SongModel songModel, final SongItemClickListener songItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songItemClickListener.onItemClick(songModel);
                }
            });
        }
    }
}
