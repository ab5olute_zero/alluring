package xyz.z3ro.alluring.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.PlaylistItemClickListener;
import xyz.z3ro.alluring.models.PlaylistModel;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder> {

    private PlaylistItemClickListener playlistItemClickListener;
    private ArrayList<PlaylistModel> playlist;

    public PlaylistAdapter(ArrayList<PlaylistModel> thePlaylist, PlaylistItemClickListener playlistItemClickListener) {
        this.playlist = thePlaylist;
        this.playlistItemClickListener = playlistItemClickListener;
    }

    @NonNull
    @Override
    public PlaylistAdapter.PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_list_item, parent, false);
        return new PlaylistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistAdapter.PlaylistViewHolder holder, int position) {
        PlaylistModel playlistModel = playlist.get(position);
        holder.playlistName.setText(playlistModel.getName());

        holder.bind(playlist.get(position), playlistItemClickListener);
    }

    @Override
    public int getItemCount() {
        return playlist.size();
    }

    public class PlaylistViewHolder extends RecyclerView.ViewHolder {
        private TextView playlistName;
        private ImageView playlistExpandButton;

        public PlaylistViewHolder(View view) {
            super(view);
            playlistName = view.findViewById(R.id.textView_Playlist);
            playlistExpandButton = view.findViewById(R.id.imageView_PlaylistExpand);
        }

        private void bind(final PlaylistModel playlistModel, final PlaylistItemClickListener playlistItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playlistItemClickListener.onItemClick(playlistModel);
                }
            });
        }
    }
}
