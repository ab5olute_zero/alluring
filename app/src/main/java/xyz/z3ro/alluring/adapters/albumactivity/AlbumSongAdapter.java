package xyz.z3ro.alluring.adapters.albumactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.customclasses.SongUtility;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class AlbumSongAdapter extends RecyclerView.Adapter<AlbumSongAdapter.AlbumSongViewHolder> {
    private SongItemClickListener songItemClickListener;
    private ArrayList<SongModel> songs;

    public AlbumSongAdapter(ArrayList<SongModel> songModels, SongItemClickListener songItemClickListener) {
        this.songs = songModels;
        this.songItemClickListener = songItemClickListener;
    }

    @NonNull
    @Override
    public AlbumSongAdapter.AlbumSongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_album_song_list_item, parent, false);
        return new AlbumSongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumSongAdapter.AlbumSongViewHolder holder, int position) {
        SongModel songModel = songs.get(position);
        holder.track.setText(String.valueOf(SongUtility.getTrackNumber(songModel.getTrackNumber())));
        holder.title.setText(songModel.getTitle());
        holder.duration.setText(String.valueOf(SongUtility.parseTime(songModel.getDuration())));
        holder.bind(songs.get(position), songItemClickListener);
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class AlbumSongViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView duration;
        public TextView track;

        public AlbumSongViewHolder(View view) {
            super(view);
            track = view.findViewById(R.id.textView_album_songNumber);
            title = view.findViewById(R.id.textView_album_songTitle);
            duration = view.findViewById(R.id.textView_album_songDuration);
        }

        public void bind(final SongModel songModel, final SongItemClickListener songItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songItemClickListener.onItemClick(songModel);
                }
            });
        }
    }
}
