package xyz.z3ro.alluring.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.interfaces.AlbumItemClickListener;
import xyz.z3ro.alluring.models.AlbumModel;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumsViewHolder> {

    private AlbumItemClickListener albumItemClickListener;
    private ArrayList<AlbumModel> albums;

    public AlbumAdapter(ArrayList<AlbumModel> theAlbums, AlbumItemClickListener albumItemClickListener) {
        albums = theAlbums;
        this.albumItemClickListener = albumItemClickListener;
    }

    @NonNull
    @Override
    public AlbumAdapter.AlbumsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_grid_item, parent, false);
        return new AlbumsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.AlbumsViewHolder holder, int position) {
        AlbumModel albumModel = albums.get(position);
        holder.albumTitle.setText(albumModel.getTitle());
        holder.artistName.setText(albumModel.getArtist());
        if (albumModel.getAlbumArt() != null)
            Picasso.get().load(new File(albumModel.getAlbumArt())).fit().placeholder(R.drawable.ic_album_black_24dp)
                    .into(holder.albumArt);
        else
            Picasso.get().load(R.drawable.ic_album_black_24dp).fit().placeholder(R.drawable.ic_album_black_24dp)
                    .into(holder.albumArt);
        holder.bind(albums.get(position), albumItemClickListener);
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class AlbumsViewHolder extends RecyclerView.ViewHolder {
        public TextView albumTitle;
        public TextView artistName;
        public ImageView albumArt;

        public AlbumsViewHolder(View view) {
            super(view);
            albumTitle = view.findViewById(R.id.textView_AlbumName);
            artistName = view.findViewById(R.id.textView_Artist_AlbumName);
            albumArt = view.findViewById(R.id.imageView_AlbumArt);
        }

        public void bind(final AlbumModel album, final AlbumItemClickListener albumItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    albumItemClickListener.onItemClick(album);
                }
            });
        }
    }
}
