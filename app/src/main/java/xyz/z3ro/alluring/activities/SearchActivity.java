package xyz.z3ro.alluring.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import xyz.z3ro.alluring.R;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    private SearchView searchView;
    private BottomAppBar bottomAppBar;
    private FloatingActionButton floatingActionButtonAttached;
    private FloatingActionButton floatingActionButtonFree;
    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        handler = new Handler();

        // Set window resizable on soft keyboard input mode
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Set SearchView
        setSearchView();

        // Set BottomAppBar with click listeners
        setBottomAppBar();
        floatingActionButtonAttached = findViewById(R.id.floatingActionButton_search1);
        floatingActionButtonFree = findViewById(R.id.floatingActionButton_search2);
        hideBottomAppBar();
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    hideBottomAppBar();
                    Log.d("SearchActivity", "Hide");
                } else {
                    showBottomAppBar();
                    Log.d("SearchActivity", "Show");
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        if (searchView.hasFocus()) {
            searchView.clearFocus();
        }
        super.onBackPressed();
    }

    /**
     * Helper function to set BottomAppBar
     */
    private void setBottomAppBar() {
        bottomAppBar = findViewById(R.id.bottomAppBar_search);
        bottomAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    /**
     * Helper function to Set SearchView
     */
    private void setSearchView() {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = findViewById(R.id.searchView_search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.requestFocus();
        searchView.setQueryRefinementEnabled(true);
    }

    /**
     * Helper function to hide bottom app bar and change FAB's position
     */
    private void hideBottomAppBar() {
        bottomAppBar.setVisibility(View.GONE);
        floatingActionButtonAttached.hide();
        floatingActionButtonFree.show();
    }

    /**
     * Helper function to show bottom app bar and set FAB's position
     */
    private void showBottomAppBar() {
        bottomAppBar.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                floatingActionButtonFree.hide();
                floatingActionButtonAttached.show();
            }
        };
        handler.postDelayed(runnable, 150);
    }
}
