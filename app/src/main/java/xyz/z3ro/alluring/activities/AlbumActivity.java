package xyz.z3ro.alluring.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.adapters.albumactivity.AlbumSongAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class AlbumActivity extends AppCompatActivity {

    private ArrayList<SongModel> songlist;
    private long albumID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        // Set transparent status bar
//        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
//        getWindow().setStatusBarColor(Color.TRANSPARENT);

        songlist = new ArrayList<SongModel>();
        Intent intent = getIntent();
        albumID = intent.getLongExtra(Constants.ALBUM_ID_KEY, 0);

        // Get songs list in album
        getSongs();

        // Set songs in the recycler view
        setSongs();

        // Get album details
        getDetails();
    }

    /**
     * Helper function to get songs in the provided album through album ID.
     */
    private void getSongs() {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.TRACK};
        String selection = "is_music != 0";
        if (albumID > 0)
            selection = selection + " and album_id = " + albumID;

        ContentResolver contentResolver = getContentResolver();
        Cursor songCursor;
        if (contentResolver != null) {
            songCursor = contentResolver.query(uri, projection, selection, null, MediaStore.Audio.Media.TRACK + " ASC");
        } else
            songCursor = null;

        if (songCursor != null && songCursor.moveToFirst()) {
            int idColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int durationColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int trackColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TRACK);

            do {
                long thisID = songCursor.getLong(idColumn);
                String thisTitle = songCursor.getString(titleColumn);
                long thisDuration = songCursor.getLong(durationColumn);
                int thisTrack = songCursor.getInt(trackColumn);
                songlist.add(new SongModel(thisID, thisTitle, thisDuration, thisTrack));
            }
            while (songCursor.moveToNext());

            songCursor.close();
        }
    }

    /**
     * Helper function to set songs in the recycler view.
     */
    private void setSongs() {

        AlbumSongAdapter albumSongAdapter = new AlbumSongAdapter(songlist, new SongItemClickListener() {
            @Override
            public void onItemClick(SongModel song) {
                Toast.makeText(AlbumActivity.this, "Now Playing " + song.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerView_album_songs);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(albumSongAdapter);
        recyclerView.setItemViewCacheSize(2);
    }

    /**
     * Helper function to get album details
     */
    private void getDetails() {
        String albumName = null;
        String artistName = null;
        int noOfSongs = 0;
        String albumArt = null;

        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM, MediaStore.Audio.Albums.ARTIST
                , MediaStore.Audio.Albums.NUMBER_OF_SONGS, MediaStore.Audio.Albums.ALBUM_ART};
        String selection = MediaStore.Audio.Albums._ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(albumID)};

        ContentResolver contentResolver = getContentResolver();
        Cursor albumCursor;

        if (contentResolver != null) {
            albumCursor = contentResolver.query(uri, projection, selection, selectionArgs, null);
        } else {
            albumCursor = null;
        }

        if (albumCursor != null && albumCursor.moveToFirst()) {
            int nameColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
            int artistColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
            int noColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);
            int artColumn = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);

            albumName = albumCursor.getString(nameColumn);
            artistName = albumCursor.getString(artistColumn);
            noOfSongs = albumCursor.getInt(noColumn);
            albumArt = albumCursor.getString(artColumn);
        }
        if (albumCursor != null)
            albumCursor.close();

        setDetails(albumName, artistName, noOfSongs, albumArt);
    }

    /**
     * Helper function to set album details
     */
    private void setDetails(String albumName, String artistName, int noOfSongs, String albumArt) {
        TextView albumTitle = findViewById(R.id.textView_album_albumTitle);
        TextView artist = findViewById(R.id.textView_album_artistName);
        TextView songsNumber = findViewById(R.id.textView_album_noOfSong);
        ImageView art = findViewById(R.id.imageView_album_albumArt);

        if (albumName != null)
            albumTitle.setText(albumName);
        if (artistName != null)
            artist.setText(artistName);
        if (noOfSongs > 1)
            songsNumber.setText(getString(R.string.number_of_songs, noOfSongs, "s."));
        else
            songsNumber.setText(getString(R.string.number_of_songs, noOfSongs, '.'));

        if (albumArt != null)
            Picasso.get().load(new File(albumArt)).placeholder(R.drawable.ic_album_grey_24dp).fit().into(art);
        else
            Picasso.get().load(R.drawable.ic_album_grey_24dp).placeholder(R.drawable.ic_album_grey_24dp).fit().into(art);
    }
}