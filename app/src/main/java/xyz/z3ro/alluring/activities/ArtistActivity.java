package xyz.z3ro.alluring.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.adapters.artistactivity.ArtistAlbumAdapter;
import xyz.z3ro.alluring.adapters.artistactivity.ArtistSongAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.AlbumItemClickListener;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.AlbumModel;
import xyz.z3ro.alluring.models.SongModel;

public class ArtistActivity extends AppCompatActivity {

    private ArrayList<SongModel> songList;
    private long artistID;

    private RecyclerView songsRecyclerView;
    private ArtistSongAdapter artistSongAdapter;

    private final String TAG = "ArtistActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        // Get artist ID
        Intent intent = getIntent();
        artistID = intent.getLongExtra(Constants.ARTIST_ID_KEY, 0);

        // Initialize song list
        songList = new ArrayList<SongModel>();

        // Set back button
        ImageButton backButton = findViewById(R.id.imageView_artist_backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // Set artist name
        setArtistName();

        // Set albums
        setAlbum();

        // Set all songs initially
        setAllSongs();

        // Set scroll listener
        scrollListener();
    }

    /**
     * Helper function to set Artist name
     */
    private void setArtistName() {
        String name = null;

        Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST};
        String selection = MediaStore.Audio.Artists._ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(artistID)};

        ContentResolver contentResolver = getContentResolver();
        Cursor artistCursor;

        if (contentResolver != null) {
            artistCursor = contentResolver.query(uri, projection, selection, selectionArgs, null);
        } else {
            artistCursor = null;
        }

        if (artistCursor != null && artistCursor.moveToFirst()) {
            int nameColumn = artistCursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
            name = artistCursor.getString(nameColumn);
        }
        if (artistCursor != null)
            artistCursor.close();

        TextView artistName = findViewById(R.id.textView_artist_artistName);
        if (name != null)
            artistName.setText(name);
    }

    /**
     * Helper function to set Album list of artist
     */
    private void setAlbum() {
        ArrayList<AlbumModel> albums = new ArrayList<AlbumModel>();

        Uri uri = MediaStore.Audio.Artists.Albums.getContentUri("external", artistID);
        String[] projection = new String[]{"_id", MediaStore.Audio.Artists.Albums.ALBUM, MediaStore.Audio.Artists.Albums.ALBUM_ART};

        ContentResolver contentResolver = getContentResolver();
        Cursor albumCursor;

        if (contentResolver != null) {
            albumCursor = contentResolver.query(uri, projection, null, null, null);
        } else
            albumCursor = null;

        if (albumCursor != null && albumCursor.moveToFirst()) {
            int idColumn = albumCursor.getColumnIndex("_id");
            int albumName = albumCursor.getColumnIndex(MediaStore.Audio.Artists.Albums.ALBUM);
            int albumArt = albumCursor.getColumnIndex(MediaStore.Audio.Artists.Albums.ALBUM_ART);

            do {
                long ID = albumCursor.getLong(idColumn);
                String name = albumCursor.getString(albumName);
                String art = albumCursor.getString(albumArt);
                albums.add(new AlbumModel(ID, name, art));
            }
            while (albumCursor.moveToNext());

            albumCursor.close();
        }
        Collections.sort(albums, new Comparator<AlbumModel>() {
            @Override
            public int compare(AlbumModel albumModel, AlbumModel t1) {
                return albumModel.getTitle().compareTo(t1.getTitle());
            }
        });

        // Set recycler view
        RecyclerView albumsRecyclerView = findViewById(R.id.recyclerView_artist_albums);
        ArtistAlbumAdapter artistAlbumAdapter = new ArtistAlbumAdapter(albums, new AlbumItemClickListener() {
            @Override
            public void onItemClick(AlbumModel album) {
                Toast.makeText(ArtistActivity.this, "" + album.getID(), Toast.LENGTH_LONG).show();
                albumSelected(album.getID(), album.getTitle());
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        albumsRecyclerView.setLayoutManager(layoutManager);
        albumsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        albumsRecyclerView.setAdapter(artistAlbumAdapter);
        albumsRecyclerView.setItemViewCacheSize(2);
    }

    /**
     * Function to call when an album is selected
     */
    private void albumSelected(long albumID, String albumName) {
        TextView albumTitle = findViewById(R.id.textView_artist_albumHeader);
        albumTitle.setText(albumName);

        // Set songs for the given album
        setSongs(albumID);
    }

    /**
     * Helper function to set songs when an album is selected
     */
    private void setSongs(long albumID) {
        ArrayList<SongModel> songList = new ArrayList<SongModel>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Media._ID,MediaStore.Audio.Media.TITLE,MediaStore.Audio.Media.DURATION};
        String selection = MediaStore.Audio.Media.ALBUM_ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(albumID)};
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";

        ContentResolver contentResolver = getContentResolver();
        Cursor songCursor;

        if (contentResolver != null) {
            songCursor = contentResolver.query(uri,projection,selection,selectionArgs,sortOrder);
        }
        else
            songCursor = null;

        if (songCursor != null && songCursor.moveToFirst()) {
            int IDColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int durationColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                long ID = songCursor.getLong(IDColumn);
                String title = songCursor.getString(titleColumn);
                long duration = songCursor.getLong(durationColumn);

                songList.add(new SongModel(ID,title,duration));
            }
            while (songCursor.moveToNext());

            songCursor.close();
        }

        artistSongAdapter.setData(songList);
    }

    /**
     * Helper function to set all songs initially
     */
    private void setAllSongs() {
        ArrayList<SongModel> songs = new ArrayList<SongModel>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION};
        String selection = MediaStore.Audio.Media.ARTIST_ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(artistID)};
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";

        ContentResolver contentResolver = getContentResolver();
        Cursor songCursor;
        if (contentResolver != null) {
            songCursor = contentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
        } else
            songCursor = null;

        if (songCursor != null && songCursor.moveToFirst()) {
            int IDColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int durationColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                long ID = songCursor.getLong(IDColumn);
                String title = songCursor.getString(titleColumn);
                long duration = songCursor.getLong(durationColumn);

                songs.add(new SongModel(ID, title, duration));
            }
            while (songCursor.moveToNext());

            songCursor.close();
        }

        // Set recycler view
        songsRecyclerView = findViewById(R.id.recyclerView_artist_songs);
        artistSongAdapter = new ArtistSongAdapter(songs, new SongItemClickListener() {
            @Override
            public void onItemClick(SongModel song) {
                Toast.makeText(ArtistActivity.this, "" + song.getID(), Toast.LENGTH_LONG).show();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);
        songsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        songsRecyclerView.setAdapter(artistSongAdapter);
        songsRecyclerView.setItemViewCacheSize(2);
    }

    /**
     * Helper function to listen to recycler view scroll events
     */
    private void scrollListener() {
        final AppBarLayout appBarLayout = findViewById(R.id.appBar_artist);
        songsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.canScrollVertically(Constants.SCROLL_DIRECTION_UP))
                    appBarLayout.setElevation(8);
                else
                    appBarLayout.setElevation(0);
            }
        });
    }
}
