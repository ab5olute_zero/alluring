package xyz.z3ro.alluring.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.z3ro.alluring.R;
import xyz.z3ro.alluring.adapters.genreactivity.GenreSongAdapter;
import xyz.z3ro.alluring.customclasses.Constants;
import xyz.z3ro.alluring.interfaces.SongItemClickListener;
import xyz.z3ro.alluring.models.SongModel;

public class GenreActivity extends AppCompatActivity {

    private ArrayList<SongModel> songList;
    private long genreID;

    private final String TAG = "GenreActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);

        // Get genre ID
        Intent intent = getIntent();
        genreID = intent.getLongExtra(Constants.GENRE_ID_KEY, 0);

        // Initialize song list
        songList = new ArrayList<SongModel>();

        // Set back button
        ImageButton backButton = findViewById(R.id.imageView_genre_backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // Set up genre name
        setUpGenreName();

        // Set up songs
        setUpSongs();
    }

    /**
     * Helper function to set up genre name
     */
    private void setUpGenreName() {
        String name = null;

        Uri uri = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.Genres.NAME};
        String selection = MediaStore.Audio.Genres._ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(genreID)};

        ContentResolver contentResolver = getContentResolver();
        Cursor genreCursor;

        if (contentResolver != null)
            genreCursor = contentResolver.query(uri, projection, selection, selectionArgs, null);
        else
            genreCursor = null;

        if (genreCursor != null && genreCursor.moveToFirst()) {
            int nameColumn = genreCursor.getColumnIndex(MediaStore.Audio.Genres.NAME);

            name = genreCursor.getString(nameColumn);
            genreCursor.close();
        }

        TextView genreName = findViewById(R.id.textView_genre_genreName);
        if (name != null)
            genreName.setText(name);
    }

    /**
     * Helper function to set up songs
     */
    private void setUpSongs() {
        Uri uri = MediaStore.Audio.Genres.Members.getContentUri("external", genreID);
        String[] projection = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION};
        String sorting = MediaStore.Audio.Media.TITLE + " ASC";

        ContentResolver contentResolver = getContentResolver();
        Cursor songCursor;

        if (contentResolver != null)
            songCursor = contentResolver.query(uri, projection, null, null, sorting);
        else
            songCursor = null;

        if (songCursor != null && songCursor.moveToFirst()) {
            int IDColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int durationColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                long ID = songCursor.getLong(IDColumn);
                String title = songCursor.getString(titleColumn);
                long duration = songCursor.getLong(durationColumn);

                songList.add(new SongModel(ID, title, duration));
            }
            while (songCursor.moveToNext());

            songCursor.close();
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerView_genre_songs);
        GenreSongAdapter genreSongAdapter = new GenreSongAdapter(songList, new SongItemClickListener() {
            @Override
            public void onItemClick(SongModel song) {
                Toast.makeText(GenreActivity.this,""+song.getTitle(),Toast.LENGTH_LONG).show();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(genreSongAdapter);
        recyclerView.setItemViewCacheSize(4);
    }
}
