package xyz.z3ro.alluring.interfaces;


import xyz.z3ro.alluring.models.SongModel;

public interface SongItemClickListener {
    void onItemClick(SongModel song);
}
