package xyz.z3ro.alluring.interfaces;

import xyz.z3ro.alluring.models.GenreModel;

public interface GenreItemClickListener {
    public void onItemClick(GenreModel genreModel);
}
