package xyz.z3ro.alluring.interfaces;

import xyz.z3ro.alluring.models.PlaylistModel;

public interface PlaylistItemClickListener {
    void onItemClick(PlaylistModel playlistModel);
}
