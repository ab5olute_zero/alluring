package xyz.z3ro.alluring.interfaces;

import xyz.z3ro.alluring.models.AlbumModel;

public interface AlbumItemClickListener {
    void onItemClick(AlbumModel album);
}
