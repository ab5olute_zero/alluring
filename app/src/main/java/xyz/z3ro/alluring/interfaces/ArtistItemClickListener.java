package xyz.z3ro.alluring.interfaces;

import xyz.z3ro.alluring.models.ArtistModel;

public interface ArtistItemClickListener {
    void onItemClick(ArtistModel artistModel);
}
